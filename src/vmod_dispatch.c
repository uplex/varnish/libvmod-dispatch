/*-
 * Copyright (c) 2017 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* for strdup() */
#define _POSIX_C_SOURCE 200809L

#include "config.h"

#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <ctype.h>
#include <stdio.h>

#include "cache/cache.h"
#include "vcl.h"
#include "vbm.h"

#undef ZERO_OBJ
#define ZERO_OBJ(to, sz) (void)memset((to), 0, (sz))

#include "vcc_if.h"

/* XXX at least for now, we need the vcc private interface */
#include "vcc_interface.h"
#define VRT_vcl_get(ctx, name) VPI_vcl_get(ctx, name)
#define VRT_vcl_rel(ctx, vcl) VPI_vcl_rel(ctx, vcl)
#define VRT_vcl_select(ctx, vcl) VPI_vcl_select(ctx, vcl)


#define ERR(ctx, msg)					\
	VRT_fail((ctx), "vmod dispatch error: " msg)

#define VERR(ctx, fmt, ...)				\
	VRT_fail((ctx), "vmod dispatch error: " fmt, __VA_ARGS__)

#define INIT_BITS (64)

#define CFUNC_PREFIX ("VGC_function_")
#define CFUNC_PREFIX_LEN (sizeof(CFUNC_PREFIX) - 1)

struct vmod_dispatch_label {
	unsigned	magic;
#define VMOD_DISPATCH_LABEL_MAGIC 0x44515cd0
	char		**string;
	VCL_VCL		*vcl;
	struct vbitmap	*bitmap;
	char		*vcl_name;
	unsigned	nvcls;
};

struct vmod_dispatch_sub {
	unsigned	magic;
#define VMOD_DISPATCH_SUB_MAGIC 0x24a617ec
	vcl_func_f	**func;
	char		**string;
	struct vbitmap	*bitmap;
	char		*vcl_name;
	unsigned	nsubs;
};

struct vmod_dispatch_vcl {
	unsigned		magic;
#define VMOD_DISPATCH_VCL_MAGIC 0x214188f2
	VTAILQ_ENTRY(vcl)	list;
	void			*dlh;
};

static struct vrt_ctx dummy_ctx = { .magic = VRT_CTX_MAGIC };

static int
check_range(VRT_CTX, VCL_INT n, unsigned max,
	    struct vbitmap * const restrict bitmap,
	    const char * restrict const type, const char * restrict const name,
	    const char * restrict const method)
{
	if (n < 0) {
		VERR(ctx, "%s.%s(%ld): n must be >= 0", name, method, n);
		return 0;
	}
	if (n >= max) {
		VERR(ctx, "%s.%s(%ld): highest %s number is %d", name, method,
		     n, type, max - 1);
		return 0;
	}
	if (!vbit_test(bitmap, n)) {
		VERR(ctx, "%s.%s(%ld): %s %ld was not added", name, method, n,
		     type, n);
		return 0;
	}
	return 1;
}

VCL_VOID
vmod_label__init(VRT_CTX, struct vmod_dispatch_label **labelp,
		 const char *vcl_name)
{
	struct vmod_dispatch_label *label;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(labelp);
	AZ(*labelp);
	AN(vcl_name);

	ALLOC_OBJ(label, VMOD_DISPATCH_LABEL_MAGIC);
	AN(label);
	*labelp = label;
	label->bitmap = vbit_new(INIT_BITS);
	AN(label->bitmap);
	label->vcl_name = strdup(vcl_name);
	AN(label->vcl_name);
	AZ(label->vcl);
	AZ(label->string);
	AZ(label->nvcls);
}

VCL_VOID
vmod_label__fini(struct vmod_dispatch_label **labelp)
{
	struct vmod_dispatch_label *label;

	if (labelp == NULL || *labelp == NULL)
		return;
	CHECK_OBJ(*labelp, VMOD_DISPATCH_LABEL_MAGIC);
	label = *labelp;
	*labelp = NULL;
	if (label->vcl != NULL) {
		for (unsigned i = 0; i < label->nvcls; i++)
			if (vbit_test(label->bitmap, i)) {
				if (label->vcl[i] != NULL)
					VRT_vcl_rel(&dummy_ctx, label->vcl[i]);
				if (label->string[i] != NULL)
					free(label->string[i]);
			}
		free(label->vcl);
	}
	if (label->bitmap != NULL)
		vbit_destroy(label->bitmap);
	if (label->vcl_name != NULL)
		free(label->vcl_name);
	FREE_OBJ(label);
}

VCL_VOID
vmod_label_add(VRT_CTX, struct vmod_dispatch_label *label, VCL_INT n,
	       VCL_STRING label_name)
{
	VCL_VCL vcl;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(label, VMOD_DISPATCH_LABEL_MAGIC);

	if ((ctx->method & VCL_MET_INIT) == 0) {
		VERR(ctx, "%s.add(%ld, %s) may only be called in vcl_init",
		     label->vcl_name, n, label_name);
		return;
	}
	if (n < 0) {
		VERR(ctx, "%s.add(%ld, %s): n must be >= 0", label->vcl_name,
		     n, label_name);
		return;
	}
	if ((vcl = VRT_vcl_get(ctx, label_name)) == NULL) {
		/*
		 * XXX: currently doesn't work, VRT_vcl_get() internally
		 * has AN(vcl);
		 */
		VERR(ctx, "%s.add(%ld, %s): unknown label", label->vcl_name, n,
		     label_name);
		return;
	}
	if (n >= label->nvcls)
		label->nvcls = n + 1;
	label->vcl = realloc(label->vcl, label->nvcls * sizeof(vcl));
	if (label->vcl == NULL) {
		VERR(ctx, "%s.add(%ld, %s): out of memory for label",
		     label->vcl_name, n, label_name);
		return;
	}
	label->vcl[n] = vcl;
	label->string = realloc(label->string,
				label->nvcls * sizeof(label_name));
	if (label->string == NULL) {
		VERR(ctx, "%s.add(%ld, %s): out of memory for string",
		     label->vcl_name, n, label_name);
		return;
	}
	label->string[n] = strdup(label_name);
	vbit_set(label->bitmap, n);
}

VCL_VOID
vmod_label_go(VRT_CTX, struct vmod_dispatch_label *label, VCL_INT n)
{
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(label, VMOD_DISPATCH_LABEL_MAGIC);

	if (ctx->req == NULL) {
		VERR(ctx, "%s.go(%ld): may only be called in client context, "
		     "not in any vcl_backend_* subroutine", label->vcl_name,
		     n);
		return;
	}
	if (!check_range(ctx, n, label->nvcls, label->bitmap, "label",
			 label->vcl_name, "go"))
		return;
	VRT_vcl_select(ctx, label->vcl[n]);
	VRT_handling(ctx, VCL_RET_VCL);
}

VCL_STRING
vmod_label_string(VRT_CTX, struct vmod_dispatch_label *label, VCL_INT n)
{
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(label, VMOD_DISPATCH_LABEL_MAGIC);

	if (!check_range(ctx, n, label->nvcls, label->bitmap, "label",
			 label->vcl_name, "string"))
		return NULL;
	AN(label->string[n]);
	return label->string[n];
}

VCL_VOID
vmod_sub__init(VRT_CTX, struct vmod_dispatch_sub **subp, const char *vcl_name)
{
	struct vmod_dispatch_sub *sub;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(subp);
	AZ(*subp);
	AN(vcl_name);

	ALLOC_OBJ(sub, VMOD_DISPATCH_SUB_MAGIC);
	AN(sub);
	*subp = sub;
	sub->bitmap = vbit_new(INIT_BITS);
	AN(sub->bitmap);
	sub->vcl_name = strdup(vcl_name);
	AN(sub->vcl_name);
	AZ(sub->func);
	AZ(sub->string);
	AZ(sub->nsubs);
}

VCL_VOID
vmod_sub__fini(struct vmod_dispatch_sub **subp)
{
	struct vmod_dispatch_sub *sub;

	if (subp == NULL || *subp == NULL)
		return;
	CHECK_OBJ(*subp, VMOD_DISPATCH_SUB_MAGIC);
	sub = *subp;
	*subp = NULL;
	if (sub->func != NULL)
		free(sub->func);
	if (sub->bitmap != NULL) {
		for (unsigned i = 0; i < sub->nsubs; i++)
			if (vbit_test(sub->bitmap, i) && sub->string[i] != NULL)
				free(sub->string[i]);
		vbit_destroy(sub->bitmap);
	}
	if (sub->vcl_name != NULL)
		free(sub->vcl_name);
	FREE_OBJ(sub);
}

VCL_VOID
vmod_sub_add(VRT_CTX, struct vmod_dispatch_sub *sub, VCL_INT n,
	     VCL_STRING subname)
{
	struct vmod_dispatch_vcl *vcl;
	char *funcname, *f;
	unsigned len, l = 0;
	vcl_func_f *vcl_func;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(sub, VMOD_DISPATCH_SUB_MAGIC);
	CAST_OBJ_NOTNULL(vcl, (void *)ctx->vcl, VMOD_DISPATCH_VCL_MAGIC);

	if (subname == NULL || *subname == '\0') {
		VERR(ctx, "%s.add(%ld): subroutine name is empty",
		     sub->vcl_name, n);
		return;
	}
	if ((ctx->method & VCL_MET_INIT) == 0) {
		VERR(ctx, "%s.add(%ld, %s) may only be called in vcl_init",
		     sub->vcl_name, n, subname);
		return;
	}
	if (n < 0) {
		VERR(ctx, "%s.add(%ld, %s): n must be >= 0", sub->vcl_name,
		     n, subname);
		return;
	}
	len = WS_ReserveAll(ctx->ws);
	if (len <= CFUNC_PREFIX_LEN) {
		VERR(ctx, "%s.add(%ld, %s): insufficient workspace for "
		     "internal C function prefix", sub->vcl_name, n, subname);
		WS_Release(ctx->ws, 0);
		return;
	}
	funcname = WS_Reservation(ctx->ws);
	strcpy(funcname, CFUNC_PREFIX);
	f = funcname + CFUNC_PREFIX_LEN;

	/* cf. VCC_PrintCName() */
	for (const char *s = subname; *s; s++) {
		if (*s < 0 || !isalnum(*s)) {
			if (l + 4 >= len) {
				l = 0;
				break;
			}
			sprintf(f, "_%02x_", *s);
			f += 4;
			l += 4;
		}
		else {
			if (l == len) {
				l = 0;
				break;
			}
			*f++ = *s;
			l++;
		}
	}
	if (l == 0) {
		VERR(ctx, "%s.add(%ld, %s): insufficient workspace for "
		     "internal C function name", sub->vcl_name, n, subname);
		WS_Release(ctx->ws, 0);
		return;
	}
	funcname[CFUNC_PREFIX_LEN + l] = '\0';
	vcl_func = dlsym(vcl->dlh, funcname);
	WS_Release(ctx->ws, 0);
	if (vcl_func == NULL) {
		VERR(ctx, "%s.add(%ld, %s): VCL subroutine not found "
		     "(C function %s): %s", sub->vcl_name, n, subname, funcname,
		     dlerror());
		return;
	}
	if (n >= sub->nsubs)
		sub->nsubs = n + 1;
	if ((sub->func = realloc(sub->func, sub->nsubs * sizeof(vcl_func)))
	    == NULL) {
		VERR(ctx, "%s.add(%ld, %s): out of memory for sub",
		     sub->vcl_name, n, subname);
		return;
	}
	sub->func[n] = vcl_func;
	sub->string = realloc(sub->string, sub->nsubs * sizeof(subname));
	if (sub->string == NULL) {
		VERR(ctx, "%s.add(%ld, %s): out of memory for string",
		     sub->vcl_name, n, subname);
		return;
	}
	sub->string[n] = strdup(subname);
	vbit_set(sub->bitmap, n);
}

VCL_VOID
vmod_sub_call(VRT_CTX, struct vmod_dispatch_sub *sub, VCL_INT n)
{
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(sub, VMOD_DISPATCH_SUB_MAGIC);

	if (!check_range(ctx, n, sub->nsubs, sub->bitmap, "sub", sub->vcl_name,
			 "call"))
		return;
	AN(sub->func[n]);
	/*
	 * NOTE: We know this is _not_ how we do things with VCL_SUB, but we
	 * only provide compatbility to transition *away* from vmod_dispatch
	 */
	sub->func[n](ctx, VSUB_STATIC, NULL);
}

VCL_STRING
vmod_sub_string(VRT_CTX, struct vmod_dispatch_sub *sub, VCL_INT n)
{
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(sub, VMOD_DISPATCH_SUB_MAGIC);

	if (!check_range(ctx, n, sub->nsubs, sub->bitmap, "sub", sub->vcl_name,
			 "string"))
		return NULL;
	AN(sub->string[n]);
	return sub->string[n];
}

VCL_STRING
vmod_version(VRT_CTX)
{
	(void) ctx;
	return VERSION;
}
