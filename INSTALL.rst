INSTALLATION
============

The VMOD is built against a Varnish installation, and the autotools
use ``pkg-config(1)`` to locate the necessary header files and other
resources for Varnish. This sequence will install the VMOD::

  > ./autogen.sh	# for builds from the git repo
  > ./configure
  > make
  > make check		# to run unit tests in src/tests/*.vtc
  > make distcheck	# run check and prepare a distribution tarball
  > sudo make install

The ``autogen.sh`` and ``configure`` steps require m4 sources from the
Varnish installation and from the autoconf archive. If you encounter
errors in those steps (such as ``Need varnish.m4 -- see
INSTALL.rst``):

* Make sure that you have a Varnish installation on the same host, and
  that its ``$PREFIX/share/aclocal`` directory can be found by
  ``aclocal(1)``. If necessary, set the ``ACLOCAL_PATH`` environment
  variable to include that path.

* If you have installed Varnish in a non-standard directory, call
  ``autogen.sh`` and ``configure`` with the ``PKG_CONFIG_PATH``
  environment variable set to include the paths where the ``.pc`` file
  can be located for ``varnishapi``. For example, when varnishd
  configure was called with ``--prefix=$PREFIX``, use::

  > PKG_CONFIG_PATH=${PREFIX}/lib/pkgconfig
  > export PKG_CONFIG_PATH

* Make sure that you have the autoconf archive installed (available as
  the package ``autoconf-archive`` on most distributions).

See `CONTRIBUTING.rst <CONTRIBUTING.rst>`_ for more details about
building from source.

By default, the vmod ``configure`` script installs the vmod in
the same directory as Varnish, determined via ``pkg-config(1)``. The
vmod installation directory can be overridden by passing the
``VMOD_DIR`` variable to ``configure``.

Other files such as the man-page are installed in the locations
determined by ``configure``, which inherits its default ``--prefix``
setting from Varnish.
